import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import './style/_index.styl'
import 'vue-multiselect/dist/vue-multiselect.min.css';

Vue.config.productionTip = false

// Import components that prefixed with App globally
const requireComponent = require.context(
  './components',
  false,
  /App[\w-]+\.(?:vue|js)$/
)
requireComponent.keys().forEach(fileName => {
  const config = requireComponent(fileName)
  const name = config.default ? config.default.name : config.name

  Vue.component(name, config.default || config)
})

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    this.$store.dispatch('init')
  }
}).$mount('#app')
