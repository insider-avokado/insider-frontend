import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import store from './store';

Vue.use(Router)

const router = new Router({
  mode: 'history',
  linkExactActiveClass: 'is-active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('./views/Dashboard.vue')
    },
    {
      path: '/brands',
      name: 'brands',
      component: () => import('./views/Brands.vue')
    },
    {
      path: '/posts',
      name: 'posts',
      component: () => import('./views/Posts.vue')
    },
    {
      path: '/settings',
      name: 'settings',
      component: () => import('./views/Settings.vue')
    },
    {
      path: '/clients/accounts',
      name: 'accounts',
      component: () => import('./views/Client.vue')
    },
    {
      // will match everything
      path: '*',
      component: Home
    }
  ]
});

router.beforeEach((to, from, next) => {
  setTimeout(() => {
    if (
      store.state.user ||
      to.name === 'home' ||
      to.name === 'accounts'
    ) {
      next();
      return;
    }
    router.push('/')
  }, 200);
})


export default router;