import Vue from 'vue'
import Vuex from 'vuex'
import { $fetch, setCookie, getCookie, deleteCookie } from './utils'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: '',
    notifications: [],
    notifyId: 0
  },
  mutations: {
    SET_USER(state, user) {
      state.user = user
    },
    ADD_NOTIFICATION(state, notify) {
      state.notifyId++
      state.notifications.push(notify);
    },
    REMOVE_NOTIFICATION(state, notify) {
      state.notifications.splice(notify, 1);
    }
  },
  actions: {
    user(context, payload) {
      return $fetch({
        path: payload.action,
        method: 'POST',
        body: payload.body
      })
        .then(res => res.json())
        .then(response => {
          context.commit('SET_USER', response)
          setCookie('user', JSON.stringify(response), 1)
          return response
        })
        .catch(error => {
          return error // handle error please
        })
    },
    init({ state, commit }) {
      if (!state.user) {
        const user = getCookie('user')
        if (user) commit('SET_USER', JSON.parse(user))
      }
    },
    logout({ commit }) {
      deleteCookie('user', '', 1);
      commit('SET_USER', '');
    },
    notify({ commit, state }, payload) {
      commit('ADD_NOTIFICATION', {
        id: state.notifyId,
        timer: 4000,
        isDismissed: false,
        type: 'success',
        translate: true,
        ...payload
      })
    },
    removeNotify({ commit }, notify) {
      commit('REMOVE_NOTIFICATION', notify);
    }
  }
})
