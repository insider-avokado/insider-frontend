export function $fetch({ path, method, body, token = null }) {
  const headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json'
  }
  if (token) headers.Authorization = `Bearer ${token}`
  return fetch(`${process.env.VUE_APP_API_URL}/${path}`, {
    method,
    headers,
    body: JSON.stringify(body)
  }).then(response => {
    if (!response.ok) {
      throw response
    }
    return response
  })
}

export function setCookie(cName, cValue, exDays) {
  let d = new Date()
  d.setTime(d.getTime() + exDays * 24 * 60 * 60 * 1000)
  let expires = 'expires=' + d.toUTCString()
  document.cookie = cName + '=' + cValue + ';' + expires + ';path=/'
}

export function getCookie(cName) {
  let name = cName + '='
  let decodedCookie = decodeURIComponent(document.cookie)
  let ca = decodedCookie.split(';')
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i]
    while (c.charAt(0) == ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ''
}

export function deleteCookie(cName) {
  document.cookie = `${cName}=; expires = Thu, 01 Jan 1970 00: 00: 01 GMT`
}
